def nabeatsu(num):
    #入力文字チェック(str型の数字に変換)
    #input()はstr型
    if type(num) is int:
        num = str(num)
    #num = str(num) if type(num) is int else num
    
    if not num.isdecimal(): 
        return 'please enter only a number'


    #3の倍数と3の付く数字時だけあほになる    
    if int(num)%3 == 0 or '3' in num:
        return 'foolow'
    else:
        return 'serious'
    
    #もっと短く
    #return 'foolow' if int(num)%3 == 0 or '3' in num else 'serious'

def intnabeatsu(num):
    #数字だけ
    num=int(num)
    if num%3 == 0:
        return 'foolow'
    else:
        while(num>0):
            if num%10 ==3:
                return 'foolow'
            num=num//10
        return 'serious'
            
if __name__ == '__main__':
    try:
        while(True):
             print(intnabeatsu(input('please enter a number :')))
    except KeyboardInterrupt:
        print('\nfinished')
 
